### 📚 Table of Contents
- Base Rules
- Bundle Structure
- App Structure
- Module File Structure

***

#### 📍 Base Rules

#### 1️⃣ App Name
Include the `name` property on every Vue instance. This helps with navigating Apps and components in the Vue devtools Chrome. The instance name will default to `<Root>` without it.

```javascript
export default el => {
  return new Vue({
    el,
    name: 'product-main',
    data () {
      return {}
    }
  })
}
```

#### 2️⃣ Template Syntax
Favor inline-template syntax to adhere to the DRY principal and to provide indexable content for SEO.

#### 3️⃣ Including Components
Components should be included using the liquid or php include tags. This will allow it to leverage the inline-template without waiting for the js to load. Many helper modules (carousel, accordion, image) have been written so they can use vanilla or Vue. Using the template include rather than a component tag provides consistent approach for both.

#### 4️⃣ Props Management
Props passed into components might come from static or dynamic Vue data. For static values the prop name is an attribute on the root element in the component.

```html
<div class="img"
  is="vue-image" inline-template
  src="{{- normal -}}"
  seo="{{- seo -}}">
```

For dynamic Vue data we need to add `v-bind:` or the shorthand `:` before the attribute name. The value should then be the name of the variable being passed down.

To allow for both scenarios use an option toggle called `dynamic_options`.

```html
{%- include 'image',
  class: nil,
  object_fit: 'contain',
  loader: false,
  dynamic_options: true,
  src: 'imageUrl',
  seo: 'title'
-%}
```

```html
<div class="img"
  is="vue-image" inline-template
  {%- if dynamic_options %}
    :src="{{- src -}}"
    :seo="{{- seo -}}"
  {%- else %}
    src="{{- src -}}"
    seo="{{- seo -}}"
  {%- endif %}>
```

#### 5️⃣ Initial Text
Provide an initial value for copy even if it will be updated client-side. This way something will display even if there's an error.

```html
<h2 v-text="title">Initial Title</h2>
```

#### 6️⃣ Debugging
Use [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd) for debugging.

***

#### 📍 Bundle Structure

```bash
# in modules/
accordion.css
accordion.liquid
accordion.js
accordion.vue.js
```

##### Modules Directory
The modules folder contains the javascript logic of modules used across the site. Typically, a module file will have a corresponding markup file (e.g. a liquid, PHP or HTML file), and a corresponding stylesheet file (e.g. a CSS or SASS file).

Vue component files should be suffixed with `.vue.js`. Some base modules might come with both a vanilla js and Vue script file. In those cases the markup file should have a conditional to identify which version to use. Without this both will initialize and add to overhead. Set the option variable `is_vue` to true at the root level of the app. Every child component will then initialize its Vue script.

```HTML
{%- assign is_vue = true -%}

<section>
  <div
    class="accordion"
    {%- if is_vue %}
      is="accordion" inline-template
    {%- else %}
      data-module="accordion"
    {%- endif -%}>
  </div>
</section>
```

***

#### 📍 App Structure
As a general guideline page sections should be initialized as a Vue app. Components inside the section should be Vue components. In situations where data needs to be shared across multiple instances a global `state` variable can be used as the central source of truth. More complex apps such as a PLP should employ vuex, Vue.js' state management library. Using vuex has the benefit of mutation tracking.

```bash
Global: Vuex or global `state` variable
Section: Vue app
Components: Vue component
```

***

#### 📍 Module File Structure
Follow standard Vue.js practices.
